﻿namespace tp_compteclient_banque;
class Program
{
    static void Main(string[] args)
    {
        string? cin; string? nom; string? prenom; string? tel;

        //Création du Premier Compte
        Console.Write("Compte 1:\n Donner le CIN :");
        cin = Console.ReadLine();
        Console.Write("Donner le Nom :");
        nom=Console.ReadLine();
        Console.Write("Donner le Prénom :");
        prenom= Console.ReadLine();
        Console.Write("Donner le numéro de télèphone :");
        tel=Console.ReadLine();

        Compte compte1 = new Compte(new Client(cin,nom,prenom,tel));
        Console.WriteLine($"Détails du compte :{compte1.Resumer()}");

        //Créditation du compte1
        Console.Write("Donner le montant à déposer :");
        double montant = Convert.ToDouble(Console.ReadLine());
        compte1.Crediter(montant);
        Console.WriteLine($"Opération éfféctuée {compte1.Resumer()}");

        //Débiter le compte 1
        Console.Write("Donner le montant à retirer :");
        double retrait = Convert.ToDouble(Console.ReadLine());
        compte1.Debiter(retrait);
        Console.WriteLine($"Opération éfféctuée {compte1.Resumer()}");


        //Création du deuxième compte 
        Console.Write("Compte 2:\n Donner le CIN :");
        cin = Console.ReadLine();
        Console.Write("Donner le Nom :");
        nom=Console.ReadLine();
        Console.Write("Donner le Prénom :");
        prenom= Console.ReadLine();
        Console.Write("Donner le numéro de télèphone :");
        tel=Console.ReadLine();

        Compte compte2 = new Compte(new Client(cin,nom,prenom,tel));
        Console.WriteLine($"Détails du compte :{compte2.Resumer()}");

        //Créditation du compte2 à partir du compte 1
        Console.WriteLine($"Crediter le compte {compte2.Code} à partir du compte {compte1.Code}");
        Console.Write($"Donner le montant à déposer: ");
        montant = Convert.ToDouble(Console.ReadLine());
        compte2.Crediter(montant, compte1);
        Console.WriteLine($"Opération bien effectuée");

        //Débiter le compte1 à partir du compte 2
        Console.WriteLine($"Débiter le compte {compte1.Code} et créditer le compte {compte2.Code}");
        Console.Write($"Donner le montant à retirer: ");
        montant = Convert.ToDouble(Console.ReadLine());
        compte1.Debiter(montant, compte2);
        Console.Write($"Opération bien effectuée");


        Console.WriteLine($"{compte1.Resumer()} {compte2.Resumer()} ");



        Console.WriteLine($"{compte2.NombreDeCompteCrees()}");
        Console.ReadKey();

    }
}
