namespace tp_compteclient_banque;


public class Client
{
    public string CIN {get;private set;}
    public string Nom {get;private set;}
    public string Prenom {get;private set;}
    public string Tel {get;private set;}


    public Client()
    {
        CIN=""; Nom="";Prenom="";Tel="";
    }
    public Client (string cin, string nom, string prenom)
    {
        Nom = nom;
        Prenom = prenom;
        CIN = cin;
    }
    public Client(string cin , string nom,string prenom,string tel):
    this(cin,nom,prenom)
    {
        Tel=tel;
    }

    public string Afficher()
    {
        return $@"
        CIN: {CIN}
        Nom : {Nom}
        Prenom : {Prenom}
        Tel : {Tel}
        ";
    }

}